	/*
	 * Gets Workflow Name
	 */
	private String workflowDefinitionsByName(String workflowName) throws IOException, JSONException {
		JSONObject wrkFlowObject = new JSONObject();
		String responseString = null;
		String wrkFlowId = null;

		try {
			String wrkFlowUrl = collibraUrl + ":" + collibraPort + collibraApiPath + "workflowDefinitions?name="+ workflowName;
			RestTemplate restTemplate = new RestTemplate(); 
			String auth = collibraUsername + ":" + collibraPassword;
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
			String authHeader = "Basic " + new String(encodedAuth);
			HttpHeaders	headers = new HttpHeaders();
			headers.set(HttpHeaders.AUTHORIZATION, authHeader);
						
			HttpEntity<String> entity = new HttpEntity<>(headers); 
			ResponseEntity<String> response = restTemplate.exchange(wrkFlowUrl, HttpMethod.GET, entity, String.class);
			responseString = response.getBody(); 

			wrkFlowObject = new JSONObject(responseString);

			JSONArray resultsArray = (JSONArray) wrkFlowObject.get("results");
			JSONObject serversObject = (JSONObject) resultsArray.get(0);
 			wrkFlowId = serversObject.getString("id");
		}
		catch (HttpStatusCodeException  e) 
		{
			log.error("Exception caught due to" + e.toString());
		}
		return wrkFlowId;
	}

	/*
	 * Invokes the Workflow
	 */
	public ResponseEntity<String> startWorkFlowInstances(String workflowname, String worflowLevel, List<String> workflowLevelId,
			Map<String, String> formProperties) throws IOException {

		ResponseEntity<String> response = null; 
		String wrkFlowId = null;
		try {

			String workflowUrl=collibraUrl + ":" + collibraPort + collibraApiPath + "workflowInstances";

			WorkFlowInstances workFlowInstances = new WorkFlowInstances();

			wrkFlowId = workflowDefinitionsByName(workflowname);
			workFlowInstances.setWorkflowDefinitionId(wrkFlowId);

			workFlowInstances.setBusinessItemIds(workflowLevelId);
			workFlowInstances.setBusinessItemType(worflowLevel);

			workFlowInstances.setFormProperties(formProperties);

			ObjectMapper mapper = new ObjectMapper();
			String resources = mapper.writeValueAsString(workFlowInstances);
			log.info(resources);			
			
			RestTemplate restTemplate = new RestTemplate(); 

			String auth = collibraUsername + ":" + collibraPassword;
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
			String authHeader = "Basic " + new String(encodedAuth);

			HttpHeaders	headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set(HttpHeaders.AUTHORIZATION, authHeader);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
						
			HttpEntity<String> entity = new HttpEntity<>(resources,headers); 
			response = restTemplate.exchange(workflowUrl, HttpMethod.POST, entity, String.class);

			int statusCode = response.getStatusCodeValue();

			if (statusCode == 201) {
				log.info("Workflow Sucessfully Triggered");
			} else {
				log.error("Workflow Triggered with Error with status code :" + statusCode);
			}

		} catch (Exception e) {
			log.error("Caught with IOException\n" + e.toString());
		}
		return response;
	}
